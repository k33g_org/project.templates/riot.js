#!/bin/bash
set -o allexport; source .env; set +o allexport

# update riot.js
wget https://raw.githubusercontent.com/riot/riot/v${RIOT_VERSION}/riot%2Bcompiler.min.js --output-document=public/js/riot+compiler.min.js

# update bulma.css
wget https://raw.githubusercontent.com/jgthms/bulma/${BULMA_VERSION}/css/bulma.min.css --output-document=public/css/bulma.min.css
