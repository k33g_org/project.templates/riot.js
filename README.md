# Play with riot.js

[![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/project.templates/riot.js)


## Get or update `riot+compiler.min.js` and `bulma.min.css`

Run `update-front-dependencies.sh`


> cf **In-Browser compilation**: https://riot.js.org/compiler/#in-browser-compilation




## Serve

To server the web application:

- run: `npm start`
- or: `python3 -m http.server --directory ./public 8080`
